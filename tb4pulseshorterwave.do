onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider in
add wave -noupdate /tb4pulseshorter/pulseshorter_i/isig
add wave -noupdate /tb4pulseshorter/pulseshorter_i/clk
add wave -noupdate /tb4pulseshorter/pulseshorter_i/nres
add wave -noupdate -divider out
add wave -noupdate /tb4pulseshorter/pulseshorter_i/osig_rising
add wave -noupdate /tb4pulseshorter/pulseshorter_i/osig_falling
add wave -noupdate -divider internal
add wave -noupdate /tb4pulseshorter/pulseshorter_i/ff1_cs
add wave -noupdate /tb4pulseshorter/pulseshorter_i/ff2_cs
TreeUpdate [SetDefaultTree]
quietly WaveActivateNextPane
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {42 ns} {882 ns}

-- Code belongs to WiSe14DT-S1T3

library work;
        use work.all;

library ieee;
        use ieee.std_logic_1164.all;


entity tb4counter is
end entity tb4counter;


architecture beh of tb4counter is
        
        signal err_s        : std_logic := '0';
        signal cnt_s        : std_logic_vector( 11 downto 0 ) := (others => '0');

        signal valLd_s      : std_logic_vector( 11 downto 0 ) := (others => '0');
        signal nLd_s        : std_logic := '0';
        signal nClr_s       : std_logic := '0';
        signal up_s         : std_logic := '0';
        signal down_s       : std_logic := '0';

        signal clk_s        : std_logic := '0';
        signal nres_s       : std_logic := '0';

        
        component sg
                port(
                        valLd        : out std_logic_vector( 11 downto 0 );
                        nLd          : out std_logic;
                        nClr         : out std_logic;
                        up           : out std_logic;
                        down         : out std_logic;
                        --
                        clk          : out std_logic;
                        nres         : out std_logic
                );--]port
        end component sg;
        for all : sg use entity work.sg4counter( beh );
        
        component counter
                port(
                        err          : out std_logic;
                        cnt          : out std_logic_vector( 11 downto 0 );
                        --
                        valLd        : in std_logic_vector( 11 downto 0 );
                        nLd          : in std_logic;
                        nClr         : in std_logic;        
                        up           : in std_logic;
                        down         : in std_logic;
                        --
                        clk          : in std_logic;
                        nres         : in std_logic
           );--]port
        end component counter;
        for all : counter use entity work.counter( beh );
        
begin
        
        sg_i : sg
                port map (
                        valLd => valLd_s,
                        nLd => nLd_s,
                        nClr => nClr_s,
                        up => up_s,
                        down => down_s,
                        --
                        clk => clk_s,
                        nres => nres_s
                )--]port
        ;--]sg_i
        
        counter_i : counter
                port map (
                        err => err_s,
                        cnt => cnt_s,
                        --
                        valLd => valLd_s,
                        nLd => nLd_s,
                        nClr => nClr_s,
                        up => up_s,
                        down => down_s,
                        --
                        clk => clk_s,
                        nres => nres_s
                )--]port
        ;--}counter_i
        
end architecture beh;


onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb4counter/nres_s
add wave -noupdate /tb4counter/clk_s
add wave -noupdate -divider in
add wave -noupdate -radix unsigned /tb4counter/valLd_s
add wave -noupdate /tb4counter/nLd_s
add wave -noupdate /tb4counter/nClr_s
add wave -noupdate /tb4counter/up_s
add wave -noupdate /tb4counter/down_s
add wave -noupdate -divider out
add wave -noupdate /tb4counter/err_s
add wave -noupdate -radix unsigned /tb4counter/cnt_s
TreeUpdate [SetDefaultTree]
WaveRestoreCursors
quietly wave cursor active 0
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ns} {1554 ns}

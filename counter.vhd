-- Code belongs to WiSe14DT-S1T3

library work;
    use work.all;

library ieee;
        use ieee.std_logic_1164.all;
        use ieee.std_logic_unsigned.all;
        use ieee.numeric_std.all;


entity counter is                                                               -- Device Under test
    port (
        err     : out   std_logic;                                              -- ERRor: invalid counter value
        cnt     : out   std_logic_vector( 11 downto 0 );                        -- CouNT
        --
        valLd   : in    std_logic_vector( 11 downto 0 );                        -- init VALue in case of LoaD
        nLd     : in    std_logic;                                              -- Not Load; low active LoaD
        nClr    : in    std_logic;                                              -- Not CLeaR; low active CLeaR
        up      : in    std_logic;                                              -- UP count command
        down    : in    std_logic;                                              -- DOWN count command
        --
        clk     : in    std_logic;                                              -- CLocK
        nres    : in    std_logic                                               -- Not RESet; low active synchronous reset
    );--]port
end entity counter;

architecture beh of counter is
        -- Ausgabesignale abgetaktet
        signal err_ns        : std_logic;
        signal cnt_ns        : std_logic_vector( 11 downto 0 );

        -- Ausgabesignale abgetaktet
        signal err_cs        : std_logic := '0';
        signal cnt_cs        : std_logic_vector( 11 downto 0 ) := (others => '0');

        -- Intern genutzte Signale
        signal valLd_cs      : std_logic_vector( 11 downto 0 ) := (others => '0');
        signal nLd_s        : std_logic;
        signal nClr_s       : std_logic;
        signal up_s         : std_logic;
        signal down_s       : std_logic;

        -- Signale für Flankenerkennung
        signal nLd_1_cs      : std_logic := '0';
        signal nLd_2_cs      : std_logic := '0';
        signal nClr_1_cs     : std_logic := '0';
        signal nClr_2_cs     : std_logic := '0';
        signal up_1_cs       : std_logic := '0';
        signal up_2_cs       : std_logic := '0';
        signal down_1_cs     : std_logic := '0';
        signal down_2_cs     : std_logic := '0';
begin
        err <= err_cs;
        cnt <= cnt_cs;

        -- Flankenerkennung
        nLd_s <= not (not nLd_1_cs and nLd_2_cs);
        nClr_s <= not (not nClr_1_cs and nClr_2_cs);
        up_s <= up_1_cs and not up_2_cs;
        down_s <= down_1_cs and not down_2_cs;

        reg:
        process (clk) is
        begin
                if clk='1' and clk'event then
                        if nres='0' then
                                err_cs <= '0';
                                cnt_cs <= (others => '0');

                                valLd_cs <= (others => '0');
                                nLd_1_cs <= '0';
                                nLd_2_cs <= '0';
                                nClr_1_cs <= '0';
                                nClr_2_cs <= '0';
                                up_1_cs <= '0';
                                up_2_cs <= '0';
                                down_1_cs <= '0';
                                down_2_cs <= '0';
                        else
                                if err_ns='1' then
                                        err_cs <= '1';
                                elsif nClr_s='0' then
                                        err_cs <= '0';
                                else --err_ns='0' oder nClr_s='1'
                                        -- nothing
                                end if;

                                cnt_cs <= cnt_ns;

                                valLd_cs <= valLd;
                                nLd_1_cs <= nLd;
                                nLd_2_cs <= nLd_1_cs;
                                nClr_1_cs <= nClr;
                                nClr_2_cs <= nClr_1_cs;
                                up_1_cs <= up;
                                up_2_cs <= up_1_cs;
                                down_1_cs <= down;
                                down_2_cs <= down_1_cs;
                        end if;
                end if;
        end process reg;

        count:
        process (nLd_s, valLd_cs, cnt_cs, up_s, down_s) is
                variable cur_v          : std_logic_vector( 12 downto 0 );
                variable compare_v      : std_logic_vector ( 1 downto 0 );
                variable add_v          : std_logic_vector( 12 downto 0 );
                variable cnt_v          : std_logic_vector( 12 downto 0 );
                variable cnt_positiv_v  : std_logic;
                variable err_v          : std_logic;
        begin
                -- cur_v setzen
                if (nLd_s = '1') then
                        cur_v := '0' & cnt_cs;
                else
                        cur_v := '0' & valLd_cs;
                end if;

                -- add_v setzen
                compare_v := up_s & down_s;

                case compare_v is
                        when "01" => -- down
                                add_v := (others => '1'); -- -1
                        when "10" => -- up
                                add_v := (0 => '1', others => '0');
                        when others => -- "00" "11"
                                add_v := (others => '0');
                end case;

                -- cnt_v berechnen
                cnt_v := cur_v + add_v;

                -- error setzen
                cnt_positiv_v := cnt_v(cnt_v'high);
                err_v := cnt_positiv_v;

                -- Variablen auf Signale setzen
                err_ns <= err_v;
                cnt_ns <= cnt_v( 11 downto 0 );

        end process count;

end architecture beh;

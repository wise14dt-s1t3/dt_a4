library work;
use work.all;

library ieee;
use ieee.std_logic_1164.all;

entity pulseshorter is
    port(
        signal osig_rising  : out std_logic;
        signal osig_falling : out std_logic;
        signal isig         : in  std_logic;
        signal clk          : in  std_logic;
        signal nres         : in  std_logic
    );
end entity pulseshorter;

architecture beh of pulseshorter is
    signal ff1_cs : std_logic := '0';
    signal ff2_cs : std_logic := '0';
begin
    osig_rising <= ff1_cs and not ff2_cs;             --rising
    osig_falling <= not ff1_cs and ff2_cs;      --falling

    reg : process(clk) is
    begin
        if clk = '1' and clk'event then
            if (nres = '0') then
                ff1_cs <= '0';
                ff2_cs <= '0';
            else
                ff1_cs <= isig;
                ff2_cs <= ff1_cs;
            end if;
        end if;
    end process;
end architecture beh;
-- Code belongs to WiSe14DT-S1T3

library work;
	use work.all;

library ieee;
	use ieee.std_logic_1164.all;


entity tb4pulseshorter is
end entity tb4pulseshorter;


architecture beh of tb4pulseshorter is
	
	signal rising_s		: std_logic := '0';
	signal falling_s	: std_logic := '0';

	signal valLd_s	: std_logic_vector( 11 downto 0 ) := (others => '0');
	signal nLd_s	: std_logic := '0';
	signal nClr_s	: std_logic := '0';
	signal up_s		: std_logic := '0';
	signal down_s	: std_logic := '0';

	signal clk_s	: std_logic := '0';
	signal nres_s	: std_logic := '0';

	
	component sg
		port(
			valLd	: out std_logic_vector( 11 downto 0 );
			nLd		: out std_logic;
			nClr 	: out std_logic;
			up		: out std_logic;
			down	: out std_logic;
			--
			clk		: out std_logic;
			nres	: out std_logic
		);--]port
	end component sg;
	for all : sg use entity work.sg4counter( beh );
	
	component pulseshorter
		port(
			signal osig_rising  : out std_logic;
			signal osig_falling : out std_logic;
			signal isig         : in  std_logic;
			signal clk          : in  std_logic;
			signal nres         : in  std_logic
		);
	end component pulseshorter;
	for all : pulseshorter use entity work.pulseshorter( beh );
	
begin
	
	sg_i : sg
		port map (
			valLd => valLd_s,
			nLd => nLd_s,
			up => up_s,
			down => down_s,
			--
			clk => clk_s,
			nres => nres_s
		)--]port
	;--]sg_i
	
	pulseshorter_i : pulseshorter
		port map (
			osig_rising  => rising_s,
            osig_falling => falling_s,
            isig         => up_s,
            clk          => clk_s,
            nres         => nres_s
		)--]port
	;--}pulseshorter_i
	
end architecture beh;
